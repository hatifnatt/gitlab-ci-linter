<!-- Empty lines are removed in gitlab ide, but TOC-generator requires them -->

<!-- toc -->

- [gitlab-ci-linter](#gitlab-ci-linter)
  * [Pre-commit hook](#pre-commit-hook)
  * [Using without pre-commit](#using-without-pre-commit)

<!-- tocstop -->

# gitlab-ci-linter
`gitlab-ci-linter` validates you `.gitlab-ci.yml` file from command-line, before you have commited wrong CI configuration to your repository!

Just as https://docs.gitlab.com/ee/ci/lint.html does, but you don't have to open browser every time.

The better way to use it - use as pre-commit hooks.

## Pre-commit hook
See [pre-commit](https://pre-commit.com) for instructions.
```bash
pip install pre-commit
# Feel .pre-commit-config.yaml as showed below
pre-commit install
pre-commit run --all-files
```

Sample `.pre-commit-config.yaml`:
```yaml
-   repo: https://gitlab.com/devopshq/gitlab-ci-linter
    rev: v1.0.4
    hooks:
    -   id: gitlab-ci-linter
```

To specify your own Gitlab server:
```yaml
-   repo: https://gitlab.com/devopshq/gitlab-ci-linter
    hooks:
    - id: gitlab-ci-linter
      args:
        - '--server'
        - 'https://gitlab.example.com'
# Use --insecure for self-signed certificate if you don't worry about security :)
# Or if you have a error: [SSL: CERTIFICATE_VERIFY_FAILED]
        - '--insecure'
```

Use GitLab private token to access api. The token must have api scope. Token can be provided in different ways:

- as argument `--private-token your-token-here`
- as environment variable `GITLAB_PRIVATE_TOKEN`
- or it can be read from file, by default `gitlab_ci_linter` will try to load token from `.gitlab_token` file  
  but you can provide your own file via argument `--private-token-file path/to/file`

```yaml
-   repo: https://gitlab.com/devopshq/gitlab-ci-linter
    hooks:
    - id: gitlab-ci-linter
      args:
        - '--server'
        - 'https://gitlab.example.com'
        # via argument
        - '--private-token'
        - 'your-token-here'
        # via file
        - '--private-token-file'
        - 'path/to/file'
```

To change a filename:
```yaml
-   repo: https://gitlab.com/devopshq/gitlab-ci-linter
    hooks:
    - id: gitlab-ci-linter
      files: '.gitlab-ci.custom.yml'
```

To check multiple CI files:
```yaml
-   repo: https://gitlab.com/devopshq/gitlab-ci-linter
    hooks:
    - id: gitlab-ci-linter
      files: 'project-.*/.gitlab-ci.yml'
```


To use a project specific api endpoint:
```yaml
-   repo: https://gitlab.com/devopshq/gitlab-ci-linter
    hooks:
    - id: gitlab-ci-linter
      files: '.gitlab-ci.custom.yml'
      args:
        - '--server'
        - 'https://gitlab.example.com'
        - '--project'
        - 'user-name/my-project'
```

## Using without pre-commit
We have no package on `pypi` yet. Use git instead:

```bash
cd yourproject

pip install git+https://gitlab.com/devopshq/gitlab-ci-linter@master
gitlab-ci-linter --help

#
gitlab-ci-linter

# Check with your on-premise instanse
gitlab-ci-linter --server https://gitlab.example.com #--insecure --private-token <GITLAB_PRIVATE_TOKEN>
```
