#!/usr/bin/env python3
import argparse
import json
import logging
import os
import ssl
import sys
import urllib.error
import urllib.parse
import urllib.request


GITLAB_TOKEN_FILE = ".gitlab_token"


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--server",
        default="https://gitlab.com",
        help="This server will check .gitlab-ci.yml",
    )
    parser.add_argument(
        "files",
        nargs="+",
        default=[".gitlab-ci.yml"],
        help="One or more Gitlab CI files to validate",
    )
    parser.add_argument(
        "-k",
        "--insecure",
        action="store_true",
        help="Allow insecure server connections when using SSL",
    )
    parser.add_argument(
        "--private-token",
        help="Use this private token to authenticate on the server",
        default=os.environ.get("GITLAB_PRIVATE_TOKEN"),
    )
    parser.add_argument(
        "--private-token-file",
        help="Read private token from specified file",
        default=GITLAB_TOKEN_FILE,
    )
    parser.add_argument(
        "--project",
        help="ID of the Gitlab project for which the private-token is authorized",
    )
    return parser.parse_args()


def main():
    args = parse_args()
    errors = 0
    for filename in args.files:
        result = gitlab_ci_linter(
            args.server, filename, args.insecure, args.project, get_private_token(args)
        )
        errors = errors + result
    return errors


def get_private_token(args):
    private_token = ""

    if args.private_token:
        return args.private_token

    # It's ok if default token file is not present
    if args.private_token_file == GITLAB_TOKEN_FILE and not os.path.isfile(
        args.private_token_file
    ):
        return None

    try:
        private_token_file = open(args.private_token_file, "r")
        private_token = private_token_file.readline().rstrip()
    except FileNotFoundError:
        print(
            f"Private token file is not found: {args.private_token_file}",
            file=sys.stderr,
        )
        return None
    finally:
        private_token_file.close()

    if not private_token:
        print("Private token is empty", file=sys.stderr)
        return None

    return private_token


def encode(input):
    input = urllib.parse.quote(input)
    return input.replace("/", "%2F")


def gitlab_ci_linter(server, filename, insecure, project, private_token=""):
    print(
        f"""
[debug] server: {server}
[debug] project: {project}
[debug] insecure: {insecure}
[debug] filename: {filename}"""
    )

    if private_token:
        print("[debug] private_token is specified")

    try:
        gitlab_ci_content = open(filename).read()
    except FileNotFoundError:
        print(f"File not found: {filename}", file=sys.stderr)
        return 1

    if project:
        project = encode(project)
        url = f"{server}/api/v4/projects/{project}/ci/lint"
    else:
        url = f"{server}/api/v4/ci/lint"
    logging.debug(f"using {url} to validate gitlab-ci.y")
    content = {"content": gitlab_ci_content}
    data = json.dumps(content).encode("utf-8")

    r = urllib.request.Request(url, data=data)

    r.add_header("Content-Type", "application/json")

    if private_token:
        r.add_header("PRIVATE-TOKEN", private_token)

    # Verify or not server certificate
    ssl_ctx = ssl.SSLContext() if insecure else None
    try:
        with urllib.request.urlopen(r, context=ssl_ctx) as gitlab:
            if gitlab.status not in range(200, 300):
                print(f"Server said {gitlab.status}: {gitlab.url}", file=sys.stderr)
                return 1

            response_raw = gitlab.read()
            response = json.loads(response_raw.decode("utf-8"))

    except urllib.error.HTTPError as err:
        print(f"Server said {err}: {url}")
        return 1

    if response.get("status") == "valid" or response.get("valid"):
        print(f"{filename} is valid")
        return 0
    else:
        print(f"{filename} is invalid (server: {server}):", file=sys.stderr)
        print("\n".join(response["errors"]), file=sys.stderr)
        return 1


if __name__ == "__main__":
    sys.exit(main())
